# TP2 MTH6601

Ce TP décrit comment exécuter une optimisation PMNE (Programmation Mathématique en Nombres
Entiers) parallèle en utilisant Open MPI (Message Passing Interface) comme protocole de transport
pour gérer la communication entre le maître et les agents. Il étudie aussi un cas de la ré-optimisation
pour le problème de partitionnement d’ensemble.

# REFERENCES

[Integral Simplex Using Decomposition for theSet Partitioning Problem, Zaghrouti et al.](https://doi.org/10.1287/opre.2013.1247)



