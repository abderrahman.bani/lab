#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <ilcplex/ilocplex.h>

using std::cerr;
using std::cout;
using std::endl;

// Print a usage message and exit.
static void usage(const char *progname) {
	cerr << "Usage: " << progname << " instance-name..." << endl;
	exit(2);
}

int main(int argc, char *argv[]) {
	if (argc <= 1)
		usage(argv[0]);

	/* Loop over all command line arguments. */
	for (int a = 1; a < argc; ++a) {
		std::cout << "\n\033[32m====================================="
				<< "====================================\033[37m\n";
		std::cout << "\t>>> Start for Instance " << argv[a] << "\n";
		std::cout << "\033[32m====================================="
				<< "=====================================\033[37m\n";

		for (int pert = 0; pert < 4; pert++) {
			try {
				IloEnv env;
				IloModel model(env);
				IloCplex cplex(model);
				//cplex.setParam(IloCplex::LBHeur, 1); // switch to local branching
				
				IloObjective obj(env);
				IloNumVarArray x(env);
				IloRangeArray cons(env);

				if (pert == 0) {

					std::stringstream instname;
					instname << argv[a] << ".mps";
					cplex.importModel(model, instname.str().c_str(), obj, x,
							cons);

				} else {
					std::cout << "\033[33m____________________________________"
							<< "_______________________________________\033[37m\n";
					std::cout << "\033[36m\t>>> Start perturbation " << pert
							<< " of instance " << argv[a] << "\n";
					std::cout << "\033[33m____________________________________"
							<< "_______________________________________\033[37m\n";

					std::stringstream instname;
					instname << argv[a] << ".pert" << pert << ".mps";
					cplex.importModel(model, instname.str().c_str(), obj, x,
							cons);
				}

				for (int v = 0; v < x.getSize(); v++) {
					model.add(IloConversion(env, x[v], ILOBOOL));
				}

				std::stringstream solfile;
				solfile << argv[a] << ".pert" << pert << ".cplex.sol";
				std::ifstream inFile;
				inFile.open(solfile.str().c_str());

				if (inFile) {
					std::cout
							<< "\033[32m\t>>> Read initial solution from file "
							<< solfile.str() << "\033[37m" << std::endl;

					std::string line;
					std::map<int, bool> xOnes; // store index of columns equal to one in initial solution
					while (getline(inFile, line)) {
						if (line.size() > 1 and line.at(0) != '#') {
							int idx = atoi(line.c_str());
							xOnes[idx] = true;
						}
					}

					IloNumArray startValX(env);
					for (int v = 0; v < x.getSize(); v++) {
						if (xOnes.find(v) != xOnes.end()) {
							startValX.add(1);
						} else {
							startValX.add(0);
						}
					}
					
					cplex.addMIPStart(x, startValX, IloCplex::MIPStartAuto); /// add initial solution
					startValX.end();

				} else {
					std::cout << "\033[33m____________________________________"
							<< "_______________________________________\033[37m\n";
					std::cout
							<< "\n\033[36m\t>>> Starting with no initial solution !!!!! \033[36m"
							<< std::endl;
					std::cout << "\033[33m____________________________________"
							<< "_______________________________________\033[37m\n";
				}

				
				if (cplex.solve()) {// solve the problem
				} 

				env.end();
			} catch (IloException& e) {
				std::cerr << "Concert Exception: " << e << endl;
			}
		}
		std::cout << "\n\033[32m====================================="
				<< "====================================\033[37m\n";
		std::cout << "\t>>> End for Instance " << argv[a] << "\n";
		std::cout << "\033[32m====================================="
				<< "=====================================\033[37m\n";
	}

	return 0;
}
