#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <ilcplex/ilocplex.h>

using std::cerr;
using std::cout;
using std::endl;
ILOSTLBEGIN

// Print a usage message and exit.
static void usage(const char *progname) {
	cerr << "Usage: " << progname << " instance-name..." << endl;
	exit(2);
}

int main(int argc, char *argv[]) {
	if (argc <= 1)
		usage(argv[0]);

	/* Loop over all command line arguments. */
	for (int a = 1; a < argc; ++a) {

		std::cout << "\n\033[32m====================================="
				<< "====================================\033[37m\n";
		std::cout << "\t>>> Start Parallel distributed MIP cplex  for Instance ";
		std::cout << argv[a] << "\n";
		std::cout << "\033[32m====================================="
				<< "=====================================\033[37m\n";

		for (int pert = 2; pert < 4; pert++) {
			try {
				
				
				IloEnv env;
				IloModel model(env);
				IloCplex cplex(model);
				IloObjective obj(env);
				IloNumVarArray x(env);
				IloRangeArray cons(env);
				
				// Load the virtual machine configuration.
      			// This will force solve() to use parallel distributed MIP.
				cplex.readVMConfig("config.vmc");
				
				cplex.setParam(IloCplex::Param::Threads, 1); //number of threads control 
				cplex.importModel(model, argv[a], obj, x, cons); // read instance file

				if (cplex.solve()) {
					// solve the problem
				}

				env.end();

			} catch (IloException& e) {
				std::cerr << "Concert Exception: " << e << endl;
			}
		}
		std::cout << "\n\033[32m====================================="
				<< "====================================\033[37m\n";
		std::cout << "\t>>> End for Instance " << argv[a] << "\n";
		std::cout << "\033[32m====================================="
				<< "=====================================\033[37m\n";
	}

	return 0;
}
