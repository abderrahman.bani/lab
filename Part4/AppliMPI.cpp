#include <stdio.h>
#include "mpi.h" // MPI
#include <ilcplex/ilocplex.h> // cplex
#include <ilconcert/iloenv.h> // concert
#include <iostream>
#include <fstream> 
#include <cstddef> // offsetof

using namespace std;
int Master = 0; // rank of master process

//==============================================================================
// Usage
//==============================================================================
static void usage(char const *program) {
	fprintf(stderr, "Usage: %s  <model>\n"
			"   Solve a specified problem \n"
			"   Arguments:\n"
			"    <model>  Model file with the model to be solved.\n"
			"   Example:\n"
			"     %s   model.mps\n", program, program);
}

//==============================================================================
// Struct to stock method result and create custom MPI_DATATYPE
//==============================================================================

typedef struct Result {
	int method;
	double mtime;
	double objvalue;
	double objlpvalue;
	double gapvalue;
} result;

int blocksCount = 5;
int blocksLength[5] = { 1, 1, 1, 1, 1 };

MPI_Datatype types[5] = { MPI_INT, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
		MPI_DOUBLE };
MPI_Aint offsets[5] = { offsetof(result, method), offsetof(result, mtime),
		offsetof(result, objvalue), offsetof(result, objlpvalue), offsetof(
				result, gapvalue) };

MPI_Datatype result_type;

//==============================================================================
// Resolution method
//==============================================================================
int solve(char * modelFileName, int method) {
	IloEnv env;
	result send;
	try {

		IloModel model(env);
		IloCplex cplex(env);
		std::stringstream slog;
		slog << modelFileName << "-method" << method << ".log";
		std::ofstream LogFile(slog.str().c_str());
		cplex.setOut(LogFile);

		if (method == 1) {

			// Parametre IloCplex::RootAlg :
			// C'est le paramètre d'algorithme racine qui définit l'option
			// d'optimiseur utilisée pour résoudre la première relaxation
			// continue (qu'elle soit la seule relaxation ou simplement
			// la première d'une série de problèmes) :
			// 1 	IloCplex::Primal
			// 2 	IloCplex::Dual
			// 3 	IloCplex::Network
			// 4 IloCplex::Barrier
			// 5 	IloCplex::Sifting

			cplex.setParam(IloCplex::RootAlg, IloCplex::Barrier);

			// Parametre  IloCplex::NodeAlg :
			// L'algorithme utilisé pour tous les autres noeuds
			// à l'exception du noeud racine
			// 1 	IloCplex::Primal
			// 2 	IloCplex::Dual
			// 3 	IloCplex::Network
			// 4 IloCplex::Barrier
			// 5 	IloCplex::Sifting

			cplex.setParam(IloCplex::NodeAlg, IloCplex::Barrier);

			// Parametre  IloCplex::NodeSel :
			//
			// 0 	Recherche avec parcours en profondeur
			// 1 	Recherche de meilleure borne ; valeur par défaut
			// 2   Recherche de la meilleure estimation
			// 3 	Recherche de la meilleure estimation alternative

			cplex.setParam(IloCplex::NodeSel, 1);

		} else if (method == 2) {

			/*
			 Modifier cette section pour specifier la methode de branchment et
			 la methode de resolution des LPs pour la methode 2
			 utilisez l'exemple de la methode 1

			 */

		} else if (method == 3) {

			/*
			 Modifier cette section pour specifier la methode de branchment et
			 la methode de resolution des LPs pour la methode 3
			 utilisez l'exemple de la methode 1

			 */

		}

		IloObjective obj;
		IloNumVarArray var(env);
		IloRangeArray rng(env);
		cplex.importModel(model, modelFileName, obj, var, rng);
		model.add(IloConversion(env, var, ILOBOOL));
		cplex.extract(model);
		IloNum start;
		start = cplex.getTime();

		if (!cplex.solve()) {
			cerr << "Failed to optimize LP" << endl;
			throw(-1);
		}

		IloNumArray vals(env);
		cplex.getValues(vals, var);

		// stock result
		send.method = method;
		send.mtime = cplex.getTime() - start;
		send.objvalue = cplex.getObjValue();
		send.objlpvalue = cplex.getBestObjValue();
		send.gapvalue = (((cplex.getObjValue() - cplex.getBestObjValue())
				/ (cplex.getBestObjValue())) * 100);

	} catch (IloException& e) {
		cerr << "Concert exception caught: " << e << endl;
	} catch (...) {
		cerr << "Unknown exception caught" << endl;
	}
	env.end();
	// Send result to Master
	MPI_Send(&send, 1, result_type, Master, 0, MPI_COMM_WORLD);
	return 0;
}  // END solve

//==============================================================================
// Main function
//==============================================================================

int main(int argc, char *argv[]) {
	int procs; //number of process
	result resu;
	MPI_Status status;
	int rank; // process rank
	MPI_Comm com;

	if (argc != 2) { // arguments control
		usage(argv[0]);
		MPI_Abort(com, -1); // stop all process
		return -1;
	}

	char* instanceFileName = argv[1];

	//__________________________________________________________________________
	/* MPI ini */
	MPI_Init(&argc, &argv);
	com = MPI_COMM_WORLD;
	MPI_Comm_size(com, &procs);
	MPI_Comm_rank(com, &rank);

	//create and commit result struct to get result
	MPI_Type_create_struct(blocksCount, blocksLength, offsets, types,
			&result_type);
	MPI_Type_commit(&result_type);

	if (rank != Master) {
		// Slaves ===============================================
		cout << "\t>>>>>  Method " << rank << " start on process " << rank
				<< endl;
		int res = solve(instanceFileName, rank);

	} else {
		// Master =============================================
		cout << "\n\n\033[34m $_-___-_$\033[31m"
				<< "   Master wait for response .......  \033[37m \n"
				<< "____________________________________________________\n";

		//  Receive response from any_process
		MPI_Recv(&resu, 1, result_type, MPI_ANY_SOURCE, MPI_ANY_TAG, com,
				&status);
		// Print  the result

		cout << "\n\033[32m >>>>> Method " << resu.method
				<< " solved the problem " << instanceFileName << "\033[37m"
				<< endl << "\033[32m >>>>> Log file is " << instanceFileName
				<< "-method" << resu.method << ".log\033[37m"
				<< "\n____________________________________________________\n";

		cout << "\n\033[34m {\n";
		cout << "\tObjective function value  = " << resu.objvalue << endl;
		cout << "\tSolution Lp value  = " << resu.objlpvalue << endl;
		cout << "\tGap value = " << resu.gapvalue << "%" << endl;
		cout << " }\033[37m\n";

		cout << "____________________________________________________\n";

		MPI_Abort(com, 0); //abort all other process
		MPI_Finalize();

		return 0;
	}

	//__________________________________________________________________________
	/* MPI Finalize */
	MPI_Finalize();
	return 0;

}

