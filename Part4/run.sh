#!/bin/bash

#=============================================================================
# chargement des modules necessaire pour la compilation et l'execution du code
#=============================================================================
module unload gcc # pour utiliser openmpi
module load openmpi/3.0.0
module load cplex-studio/12.8.0.0

#=============================================================================
# Compilation du code
#=============================================================================
make clean
make
echo "Fin de la compilation du code"
#=============================================================================
# Execution de toutes instances sur les 4 machines :
#      gerad-1770,gerad-1783,gerad-1773,gerad-1792 
#=============================================================================

INSTANCES="S1.lp
S2.lp
S3.lp"

# Executer le programme sur chaque instance
for instance in $INSTANCES
do
	echo "**********************************************************"
	echo "Lancement de l'instance : $instance"
	echo "**********************************************************"
	echo ""
	/home/x86_64-unknown-linux_ol7-gnu/openmpi-3.0.0/bin/mpirun -host gerad-1770,gerad-1783,gerad-1773,gerad-1792 --mca btl_tcp_if_include enp0s31f6 ./AppliMPI ./instances/$instance 2> /dev/null
	echo ""
	echo "----------------------------------------------------------"
	echo "Fin de l'instance $instance "
	echo "----------------------------------------------------------"
	
done

