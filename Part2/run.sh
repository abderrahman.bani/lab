#!/bin/bash

#=============================================================================
# chargement des modules necessaire pour la compilation et l'execution du code
#=============================================================================
module unload gcc
module load openmpi
module load cplex-studio

#=============================================================================
# Compilation du code
#=============================================================================
make clean
make
echo "Fin de la compilation du code"


./main ./instances/S1.lp ./instances/S2.lp ./instances/S3.lp

